//
//  Questions.swift
//  Question
//
//  Created by Tanveer Bashir on 10/16/15.
//  Copyright © 2015 Tanveer Bashir. All rights reserved.
//

import Foundation

struct Questions {
    var capitalCitties = [String]()
    let file = NSBundle.mainBundle().pathForResource("stateCapitals", ofType: "json")
    
    init(){
        self.capitalCitties = capitals(parsJSON())
    }
    
    func parsJSON() -> [String : [String : String]] {
        let data = NSData(contentsOfFile: file!)
        let dictionary = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! [String :[String : String]]
        return dictionary
    }
    
    func capitals(dictionary: [String : [String : String]] ) -> [String]{
        var capitals = [String]()
        var capital = ""
        for (_, value) in dictionary {
            if let cap = value.keys.sort().first{
                if cap == "capital"{
                    capital = cap
                }
            }
        }
        
        for (_ ,v) in dictionary {
            if (v[capital] != nil) {
                capitals.append((v[capital]! as String))
            }
        }
        
        return capitals
    }

}