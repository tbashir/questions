//
//  ViewController.swift
//  Question
//
//  Created by Tanveer Bashir on 10/16/15.
//  Copyright © 2015 Tanveer Bashir. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var firstAnswer: UIButton!
    @IBOutlet weak var secondAnswer: UIButton!
    @IBOutlet weak var thirdAnswer: UIButton!
    @IBOutlet weak var forthAnswer: UIButton!
    @IBOutlet weak var nextQuestion: UIButton!
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var wrongLabel: UILabel!
    var num:Int?
    var question: Questions!
    var stateAndCapitals:[String:String] = [:]
    var state = [String]()
    var capital = [String]()
    var reservedStateArray = [String]()
    var score = 0
    var wrong = 0
    var numbers = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeDictionary()
        gameSetup()

    }
    
    func makeDictionary(){
        question = Questions()
        let dic = question.parsJSON()
        for ( _ , value) in dic {
            stateAndCapitals.updateValue(value["capital"]!, forKey: value["name"]!)
        }
        
        for (s, c) in stateAndCapitals {
            state.append(s)
            capital.append(c)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //: MARK: Multiple choice answer buttons
    
    @IBAction func firstAnswerButton(sender: UIButton) {
        let buttonTitle = sender.titleLabel!.text
        checkAnswer(buttonTitle!)
    }
    
    @IBAction func secondAnswerButton(sender: UIButton) {
        let buttonTitle = sender.titleLabel!.text
        checkAnswer(buttonTitle!)
    }
    
    @IBAction func thirdAnswerButton(sender: UIButton) {
        let buttonTitle = sender.titleLabel!.text
        checkAnswer(buttonTitle!)
    }
    
    @IBAction func forthAnswerButton(sender: UIButton) {
        let buttonTitle = sender.titleLabel!.text
        checkAnswer(buttonTitle!)
    }
    
    // MARK: Next question button
    
    @IBAction func startGame(sender: UIButton) {
        for (s, _) in stateAndCapitals {
            state.append(s)
        }

        gameSetup()
        scoreLabel.text = ""
        wrongLabel.text = ""
        score = 0
        wrong = 0
    }
    
    // MARK: Helper functions
    
    func gameSetup(){
        changeBUttonState()
        reservedStateArray = state
        num = Int(arc4random_uniform(UInt32(state.count))) % 50
        questionLabel.text = "What is the capital of \(state[num!])"
        firstAnswer.setTitle("\(stateAndCapitals[state[num!]]!)", forState: .Normal)
        secondAnswer.setTitle("\(stateAndCapitals[state[Int(arc4random_uniform(UInt32(state.count)))]]!)", forState: .Normal)
        thirdAnswer.setTitle("\(stateAndCapitals[state[Int(arc4random_uniform(UInt32(state.count)))]]!)", forState: .Normal)
        forthAnswer.setTitle("\(stateAndCapitals[state[Int(arc4random_uniform(UInt32(state.count)))]]!)", forState: .Normal)
    }
    
    
    
    
    @IBAction func nextQuestionButton(sender: UIButton) {
        firstAnswer.enabled = true
        secondAnswer.enabled = true
        thirdAnswer.enabled = true
        forthAnswer.enabled = true
        //print("\(state.removeAtIndex(num!)) | \(state.count) | \(num!)")
        
        if state.count <= 0 {
            questionLabel.text = "Game Over!"
            firstAnswer.enabled = false
            secondAnswer.enabled = false
            thirdAnswer.enabled = false
            forthAnswer.enabled = false
            nextQuestion.hidden = true
            startGameButton.hidden = false
            
        } else {
            gameSetup()
        }
    }
    
    func changeBUttonState(){
        startGameButton.hidden = true
        nextQuestion.hidden = false
        firstAnswer.enabled = true
        secondAnswer.enabled = true
        thirdAnswer.enabled = true
        forthAnswer.enabled = true
    }
    
    func checkAnswer(sender: String) {
        
        if sender == stateAndCapitals[state[num!]]! {
            questionLabel.text = "Correct"
            scoreLabel.text = "\(++score)"
            firstAnswer.enabled = false
            secondAnswer.enabled = false
            thirdAnswer.enabled = false
            forthAnswer.enabled = false
            
        } else {
            
           questionLabel.text = "Wrong!" + "\nCorrect Answer is '\(stateAndCapitals[state[num!]]!)' "
            wrongLabel.text = "\(++wrong)"
            firstAnswer.enabled = false
            secondAnswer.enabled = false
            thirdAnswer.enabled = false
            forthAnswer.enabled = false
           
        }
    }
}

